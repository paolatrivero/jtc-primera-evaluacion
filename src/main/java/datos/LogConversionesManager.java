package datos;

//import java.io.OutputStream;
//import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
//import java.sql.Date;
//import java.util.Date;
import java.util.List;
import utils.JdbcUtil;
import utils.LogUtil;

/**
 * Clase LogConversionesManager. 
 * Implementa las operaciones sobre la tabla LOG_CONVERSIONES
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class LogConversionesManager {
    
    /**
     * Retorna la cantidad de registros en la tabla o -1 en caso de error
     * @return Nro de registros o -1 en caso de error
     */
    public int getCantidadRegistros() {
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {
            int nro = -1;
            
            try {
                Statement stm = con.createStatement();
                ResultSet rs = stm.executeQuery("select count(*) from LOG_CONVERSIONES");
                if (rs != null) {
                    if (rs.next()) {
                        nro = rs.getInt(1);
                    }
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
                        
            return nro;
        } else {
            return -1;
        }        
    }

    /**
     * Metodo que retorna el listado completo de registros de la tabla en BD
     * @return Lista de LogConversiones
     */
    public List<LogConversiones> getAll() {
        List<LogConversiones> resp = new ArrayList<LogConversiones>();
        
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {            
            try {
                Statement stm = con.createStatement();
                ResultSet rs = stm.executeQuery("select * from LOG_CONVERSIONES");
                if (rs != null) {
                    while (rs.next()) {
                        LogConversiones lc = new LogConversiones(rs.getString("nombre_thead"), rs.getString("ip_cliente"), rs.getInt("msg_request"), rs.getString("msg_response"));
                        /*
                         * TODO: #Listo Lista Pendiente de realizar el mapeo Objeto Relacional
                         * Ejemplo
                         *   lc.setCampoXX(rs.getCampoXX(1));
                         */
                        
                        lc.setNombre_thead("nombre_thead");
                        lc.setIp_cliente("ip_cliente");
                        lc.setFecha_hora(lc.getFecha_hora());
                        lc.setMsg_request(lc.getMsg_request());
                        lc.setMsg_response("msg_response");
                        
                        resp.add(lc);
                    }
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
        }
        
        return resp;
    }
    
    /**
     * Busca un registro por su ID
     * @param id ID del registro
     * @return Registro con ID o NULL si no existe
     */
    public LogConversiones buscarPorID(int id) {
        LogConversiones lc = null;
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {
            try {
                PreparedStatement stm = con.prepareStatement("select * from log_conversiones where id_mensaje = ?");
                stm.setInt(1, id);
                ResultSet rs = stm.executeQuery();
                if (rs != null) {
                    if (rs.next()) {
                    	
                    	lc = new LogConversiones(rs.getString("nombre_thead"), rs.getString("ip_cliente"), rs.getInt("msg_request"), rs.getString("msg_response"));
                        /*
                         * TODO: #listo buscar Pendiente de realizar el mapeo Objeto Relacional
                         * Ejemplo
                         *   lc.setCampoXX(rs.getCampoXX(1));
                         */
                    	lc.setId_mensaje(rs.getInt("id_mensaje"));
                    	lc.setNombre_thead("nombre_thead");
                        lc.setIp_cliente("ip_cliente");
                        lc.setFecha_hora(lc.getFecha_hora());
                        lc.setMsg_request(lc.getMsg_request());
                        lc.setMsg_response("msg_response");
                    }
                }
                con.close();
            } catch (Exception ex) {
                LogUtil.ERROR("Error al ejecutar consulta", ex);
            }
                        
            return lc;
        } else {
            return null;
        }
    }
    
    /**
     * Inserta un nuevo registro en tabla 
     * @param LogConversiones
     */
    public void insertarNuevoRegistro(LogConversiones nuevoRegistro) {
    	
    	Connection con = JdbcUtil.getInstance().getConnection();
        
        if (con != null) {            
            try {
            	/* TODO #Listo Insertar
            	 * 4
            	 * con las funciones get del objeto nuevoRegistro
            	 */

            	PreparedStatement stm = con.prepareStatement("insert into log_conversiones (nombre_thread,ip_cliente,fecha_hora,msg_request,msg_response) values (?,?,?,?,?)");
                stm.setString(1, nuevoRegistro.getNombre_thead()); 
                stm.setString(2, nuevoRegistro.getIp_cliente()); 
                stm.setTimestamp(3,new Timestamp(nuevoRegistro.getFecha_hora().getTime()));
                //stm.setDate(3, nuevoRegistro.getFecha_hora()); // TODO ERROR EN DATE
                stm.setInt(4, nuevoRegistro.getMsg_request()); 
                stm.setString(5, nuevoRegistro.getMsg_response()); 
                
                int rs = stm.executeUpdate();
                if (rs == 1) {
                    LogUtil.INFO("Registro creado exitosamente: " + nuevoRegistro);
                }
                
                con.commit();
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
        }
    }

    /**
     * Elimina el registro con ID recibido
     * @param codigo ID del registro a eliminar
     */
    public void eliminarRegistro(int id) {
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {            
            try {
                PreparedStatement stm = con.prepareStatement("delete from log_conversiones where id_mensaje = ?");
                stm.setInt(1, id);
                
                int rs = stm.executeUpdate();
                if (rs == 1) {
                    LogUtil.INFO("Registro eliminado exitosamente");
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
        }
    }
    
} //Fin de clase Libro
