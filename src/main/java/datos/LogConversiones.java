package datos;

import java.util.Date;
//import java.sql.Date;

/**
 * Clase LogConversiones. Representa a 1 registro de la tabla LOG_CONVERSIONES
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class LogConversiones {
    
    //TODO: #Listo constructor Crear estructura de la clase, constructor, getters y setters
    private Integer id_mensaje;
    private String nombre_thead;
    private String ip_cliente;
    private Date fecha_hora;
    private Integer msg_request;
    private String msg_response;
    
    public LogConversiones(String nombreThread, String ipCliente, int msgRequest, String msgResponse) {
      
      this.setNombre_thead(nombreThread);
      this.setIp_cliente(ipCliente);
      this.fecha_hora = new Date();
      this.setMsg_request(msgRequest);
      this.setMsg_response(msgResponse);
    }    
    public Integer getId_mensaje() {
		return id_mensaje;
	}
	public void setId_mensaje(Integer id_mensaje) {
		this.id_mensaje = id_mensaje;
	}
	public String getNombre_thead() {
		return nombre_thead;
	}
	public void setNombre_thead(String nombre_thead) {
		this.nombre_thead = nombre_thead;
	}
	public String getIp_cliente() {
		return ip_cliente;
	}
	public void setIp_cliente(String ip_cliente) {
		this.ip_cliente = ip_cliente;
	}
	public Date getFecha_hora() {
		return fecha_hora; 
	}
	public void setFecha_hora(Date fecha_hora) {
		this.fecha_hora = fecha_hora;		
	}
	public Integer getMsg_request() {
		if (msg_request == null){
			return 0;
		}else{
		return msg_request;
		}
	}
	public void setMsg_request(Integer msg_request) {
		this.msg_request = msg_request;
	}
	public String getMsg_response() {
		return msg_response;
	}
	public void setMsg_response(String msg_response) {
		this.msg_response = msg_response;
	}
    
} //Fin de clase
