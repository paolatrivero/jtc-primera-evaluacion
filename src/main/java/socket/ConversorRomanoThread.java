package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
//import java.net.ServerSocket;
import java.net.Socket;

import datos.*;
import romano.ConversorRomano;
import utils.LogUtil;

public class ConversorRomanoThread extends Thread {
    
	//Referencia al cliente que sera atendido
    private Socket cliente;
    private int contador;
    
    public ConversorRomanoThread(Socket cliente, int contador) {
        this.contador = contador;
    	this.cliente = cliente;
    }
    
    @Override
    public void run() {
    	LogUtil.INFO("Nueva peticion recibida...");
        //TODO: Pendiente de implementacion de logica que parsea peticion, procesa request y almacena resultado en la bd
         try {
              
                //cuando llegue a esta linea, es porque la anterior se ejecuto y avanzo
                //lo cual quiere decir que algun cliente se conecto a este servidor
                //obtenemos el OutputStream del cliente, para escribirle algo
                OutputStream clientOut = cliente.getOutputStream();
                PrintWriter pw = new PrintWriter(clientOut, true);
                
                //obtenemos el InputStream del cliente, para leer lo que nos dice
                InputStream clientIn = cliente.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));
                
                //Leemos la primera linea del mensaje recibido
                String mensajeRecibido = br.readLine();
                
                //Variables de la tabla
                String nombre;
                InetAddress address;
                String ip_cliente;
                int numero;
                String msg_response;
                LogConversiones bean;         
                
              //verificamos que viene en el mensaje
				if (mensajeRecibido != null) {
					
					if (mensajeRecibido.trim().equalsIgnoreCase("fin")) {
						
						// Nos enviaron la palabra fin, debemos parar el
						// servidor rompiendo el while
						pw.println("Muchas gracias por su visita. Estamos a las ordenes!.");
						System.exit(0);
					} else if (mensajeRecibido.trim().startsWith("conv")) {
						// Nos enviaron la palabra conv debemos sacar el nro.
						// se puede hacer de varias maneras, una simple puede
						// ser:
						String strnum = mensajeRecibido.substring(4);
						numero = Integer.parseInt(strnum.trim());
						
						try {
							
							msg_response = "El valor convertido es: " + ConversorRomano.decimal_a_romano(numero);
							pw.println(msg_response);
							
							
						} catch (Exception e) {
							msg_response = "Hubo un error: " + e.getMessage();
						}
						
						// cliente.getRemoteSocketAddress().toString();
						address = cliente.getLocalAddress();
						ip_cliente = address.getHostAddress().toString();
						nombre = "Hilo nro "+contador;
						//Cargamos el bean
						bean = new LogConversiones(nombre, ip_cliente, numero, msg_response);
						//Se guarda el Log de Conversiones
						LogConversionesManager lcm = new LogConversionesManager();
						lcm.insertarNuevoRegistro(bean);
						pw.println("Procesado");
					}
				} else {
					pw.println("Mensaje recibido no valido.. reintente por favor!!");
				}
                                
                //cerramos conexion con el cliente luego de cerrar los flujos
                clientIn.close();
                clientOut.close();                
                cliente.close();
            } catch (IOException ie) {
                System.out.println("Error al procesar socket. " + ie.getMessage());
            }
        
    	LogUtil.INFO("Finalizando atencion de la peticion recibida...");
    }

}
